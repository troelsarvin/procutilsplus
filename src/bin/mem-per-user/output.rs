use serde::Serialize;
use std::{fmt, io};
use time::{format_description::FormatItem, macros::format_description, OffsetDateTime};

use crate::{err_die, JsonSumResult, MemUnit, SumResult};

pub static DEFAULT_OUTPUT_FORMAT_STRING: &str = "oneline_ts";
pub static DEFAULT_OUTPUT_FORMAT_VAL: OutFormat = OutFormat::OnelineTs;

static DT_FORMAT: &[FormatItem<'static>] =
    format_description!("[year]-[month]-[day]T[hour]:[minute]:[second].[subsecond digits:2]");

#[derive(Clone, Debug, PartialEq, Serialize)]
pub enum OutFormat {
    CsvHeader,
    Csv,
    Hr,
    HrTs,
    Json,
    OnelineTs,
    Oneline,
}

impl Default for OutFormat {
    fn default() -> Self {
        DEFAULT_OUTPUT_FORMAT_VAL.clone()
    }
}

pub fn format_display_value(unit_out: &MemUnit, value_b: &u64) -> String {
    match unit_out {
        MemUnit::B => (*value_b).to_string(),
        MemUnit::KiB => (*value_b / 1024).to_string(),
        MemUnit::MiB => (*value_b / (1024 * 1024)).to_string(),
        MemUnit::GiB => (*value_b / (1024 * 1024 * 1024)).to_string(),
    }
}

fn print_json(jres: &JsonSumResult) -> serde_json::Result<()> {
    let j = serde_json::to_string(&jres)?;
    println!("{}", j);
    Ok(())
}

fn print_csv(res: &SumResult) -> Result<(), Box<dyn std::error::Error>> {
    let mut wtr = csv::Writer::from_writer(io::stdout());
    if res.opts.format == OutFormat::CsvHeader {
        wtr.write_record([
            "timestamp",
            "num_procs",
            "unit",
            "vmhwm",
            "vmrss",
            "rssanon",
            "rssfile",
            "rssshmem",
            "dev_shm",
            "hugetlbpages",
            "pss",
            "pss_anon",
            "pss_file",
            "pss_shmem",
        ])?;
    }
    let ts = get_timestamp_formatted();
    let pss_str: String = match &res.pss_b {
        Some(pss_b) => format_display_value(&res.opts.unit, pss_b),
        None => "?".to_string(),
    };
    let pss_anon_str: String = match &res.pss_anon_b {
        Some(pss_anon_b) => format_display_value(&res.opts.unit, pss_anon_b),
        None => "?".to_string(),
    };
    let pss_file_str: String = match &res.pss_file_b {
        Some(pss_file_b) => format_display_value(&res.opts.unit, pss_file_b),
        None => "?".to_string(),
    };
    let pss_shmem_str: String = match &res.pss_shmem_b {
        Some(pss_shmem_b) => format_display_value(&res.opts.unit, pss_shmem_b),
        None => "?".to_string(),
    };
    wtr.serialize([
        ts,
        res.num_procs.to_string(),
        res.opts.unit.to_string(),
        res.vmhwm_b.to_string(),
        res.vmrss_b.to_string(),
        res.rssanon_b.to_string(),
        res.rssfile_b.to_string(),
        res.rssshmem_b.to_string(),
        res.dev_shm_b.to_string(),
        res.hugetlbpages_b.to_string(),
        pss_str,
        pss_anon_str,
        pss_file_str,
        pss_shmem_str,
    ])?;
    wtr.flush()?;
    Ok(())
}

pub fn get_timestamp_formatted() -> String {
    let now_local = OffsetDateTime::now_local().expect("Could not determine local time(?!)");
    let now_formatted = now_local
        .format(DT_FORMAT)
        .expect("Weird fatal error: Could not format current time");
    now_formatted.to_string()
}

pub fn fmt_hr(f: &mut fmt::Formatter, res: &SumResult, ts: bool) -> fmt::Result {
    if ts {
        let now_local = OffsetDateTime::now_local().expect("Could not determine local time(?!)");
        let now_formatted = now_local
            .format(DT_FORMAT)
            .expect("Weird fatal error: Could not format current time");
        let _ = writeln!(f, "time        : {}", now_formatted);
    }
    let _ = writeln!(f, "num_procs   : {}", &res.num_procs);
    let _ = writeln!(f, "unit        : {}", &res.opts.unit);
    let _ = writeln!(
        f,
        "vmhwm       : {}",
        format_display_value(&res.opts.unit, &res.vmhwm_b)
    );
    let _ = writeln!(
        f,
        "vmrss       : {}",
        format_display_value(&res.opts.unit, &res.vmrss_b)
    );
    let _ = writeln!(
        f,
        "rssanon     : {}",
        format_display_value(&res.opts.unit, &res.rssanon_b)
    );
    let _ = writeln!(
        f,
        "rssfile     : {}",
        format_display_value(&res.opts.unit, &res.rssfile_b)
    );
    let _ = writeln!(
        f,
        "rssshmem    : {}",
        format_display_value(&res.opts.unit, &res.rssshmem_b)
    );
    let _ = writeln!(
        f,
        "dev_shm     : {}",
        format_display_value(&res.opts.unit, &res.dev_shm_b)
    );
    let _ = writeln!(
        f,
        "hugetlbpages: {}",
        format_display_value(&res.opts.unit, &res.hugetlbpages_b)
    );
    if let Some(pss_b) = &res.pss_b {
        let _ = writeln!(
            f,
            "pss         : {}",
            format_display_value(&res.opts.unit, pss_b)
        );
    } else {
        let _ = writeln!(f, "pss         : ?");
    }
    if let Some(pss_anon_b) = &res.pss_anon_b {
        let _ = writeln!(
            f,
            "pss_anon    : {}",
            format_display_value(&res.opts.unit, pss_anon_b)
        );
    } else {
        let _ = writeln!(f, "pss_anon    : ?, ");
    }
    if let Some(pss_file_b) = &res.pss_file_b {
        let _ = writeln!(
            f,
            "pss_file    : {}",
            format_display_value(&res.opts.unit, pss_file_b)
        );
    } else {
        let _ = writeln!(f, "pss_file    : ?, ");
    }
    if let Some(pss_shmem_b) = &res.pss_shmem_b {
        let _ = writeln!(
            f,
            "pss_shmem   : {}",
            format_display_value(&res.opts.unit, pss_shmem_b)
        );
    } else {
        let _ = writeln!(f, "pss_shmem   : ?");
    }
    Ok(())
}

pub fn fmt_oneline(f: &mut fmt::Formatter, res: &SumResult, ts: bool) -> fmt::Result {
    if ts {
        let now_local = OffsetDateTime::now_local().expect("Could not determine local time(?!)");
        let now_formatted = now_local
            .format(DT_FORMAT)
            .expect("Weird fatal error: Could not format current time");
        let _ = write!(f, "time: {}, ", now_formatted);
    }
    let _ = write!(f, "num_procs: {}, ", &res.num_procs);
    let _ = write!(f, "unit: {}, ", &res.opts.unit);
    let _ = write!(
        f,
        "vmhwm: {}, ",
        format_display_value(&res.opts.unit, &res.vmhwm_b)
    );
    let _ = write!(
        f,
        "vmrss: {}, ",
        format_display_value(&res.opts.unit, &res.vmrss_b)
    );
    let _ = write!(
        f,
        "rssanon: {}, ",
        format_display_value(&res.opts.unit, &res.rssanon_b)
    );
    let _ = write!(
        f,
        "rssfile: {}, ",
        format_display_value(&res.opts.unit, &res.rssfile_b)
    );
    let _ = write!(
        f,
        "rssshmem: {}, ",
        format_display_value(&res.opts.unit, &res.rssshmem_b)
    );
    let _ = write!(
        f,
        "dev_shm: {}, ",
        format_display_value(&res.opts.unit, &res.dev_shm_b)
    );
    let _ = write!(
        f,
        "hugetlbpages: {}, ",
        format_display_value(&res.opts.unit, &res.hugetlbpages_b)
    );
    if let Some(pss_b) = &res.pss_b {
        let _ = write!(f, "pss: {}, ", format_display_value(&res.opts.unit, pss_b));
    } else {
        let _ = write!(f, "pss: ?, ");
    }
    if let Some(pss_anon_b) = &res.pss_anon_b {
        let _ = write!(
            f,
            "pss_anon: {}, ",
            format_display_value(&res.opts.unit, pss_anon_b)
        );
    } else {
        let _ = write!(f, "pss_anon: ?, ");
    }
    if let Some(pss_file_b) = &res.pss_file_b {
        let _ = write!(
            f,
            "pss_file: {}, ",
            format_display_value(&res.opts.unit, pss_file_b)
        );
    } else {
        let _ = write!(f, "pss_file: ?, ");
    }
    if let Some(pss_shmem_b) = &res.pss_shmem_b {
        let _ = write!(
            f,
            "pss_shmem: {}",
            format_display_value(&res.opts.unit, pss_shmem_b)
        );
    } else {
        let _ = write!(f, "pss_shmem: ?");
    }
    Ok(())
}

pub fn present_result(sumres: SumResult) {
    if sumres.opts.format == OutFormat::Csv || sumres.opts.format == OutFormat::CsvHeader {
        if let Err(e) = print_csv(&sumres) {
            let msg = format!("Could not output CSV: {e}");
            err_die(msg);
        }
    } else if sumres.opts.format == OutFormat::Json {
        let jres: JsonSumResult = <SumResult as Into<JsonSumResult>>::into(sumres);
        if let Err(e) = print_json(&jres) {
            let msg = format!("Could not output JSON: {e}");
            err_die(msg);
        }
    } else {
        println!("{}", sumres);
    }
}
