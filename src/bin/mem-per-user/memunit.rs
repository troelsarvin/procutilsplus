use serde::Serialize;
use std::{fmt, str::FromStr};

static DEFAULT_OUTPUT_UNIT: &str = "KiB";

#[derive(Clone, Debug, PartialEq, Serialize)]
pub enum MemUnit {
    B,
    KiB,
    MiB,
    GiB,
}

impl FromStr for MemUnit {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, ()> {
        if s == "B" {
            Ok(MemUnit::B)
        } else if s == "KiB" {
            Ok(MemUnit::KiB)
        } else if s == "MiB" {
            Ok(MemUnit::MiB)
        } else if s == "GiB" {
            Ok(MemUnit::GiB)
        } else {
            Err(()) // Clap should make sure we never end up here
        }
    }
}

impl fmt::Display for MemUnit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MemUnit::B => write!(f, "B"),
            MemUnit::KiB => write!(f, "KiB"),
            MemUnit::MiB => write!(f, "MiB"),
            MemUnit::GiB => write!(f, "GiB"),
        }
    }
}

impl Default for MemUnit {
    fn default() -> Self {
        // unwrap OK, because the input value is tightly controlled by above constant
        Self::from_str(DEFAULT_OUTPUT_UNIT).unwrap()
    }
}
