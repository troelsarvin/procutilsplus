use clap::{arg, command, crate_version, value_parser, ArgMatches};
use nix::unistd::User;
use serde::Serialize;
use std::{os::unix::fs::MetadataExt, str::FromStr, thread::sleep, time::Duration};
use walkdir::WalkDir;

use memunit::MemUnit;
pub mod memunit;

use result::{JsonSumResult, SumResult};
pub mod result;

use output::*;
pub mod output;

#[cfg(test)]
mod test;

static ABOUT: &str = "Finds processes owned by the indicated user and summarizes their
memory usage. If no user argument is passed, processes for all users
are accounted for.

PSS means proportional set size, and it is a somewhat heavy operation to perform for
the kernel; thus, it's only available for root and sometimes the process owner.
If PSS values are gathered: A policy of 'require' means that the tool will exit with
an error, if permissions prevent gathering; otherwise, PSS values will simply not be
gathered/printed.

Output formats:
 csv_header: CSV output with an initial line of headers
 csv       : CSV output, no headers
 hr        : Multiple-line, human friendly, no timestamp
 hr_ts     : Multiple-line, human friendly, with timestamp
 json      : JSON output
 oneline   : All on one line, no timestmap
 oneline_ts: All on one line, including timestamp

For CSV and JSON output, the unit cannot be changed from the default.
";

static DEFAULT_OUTPUT_UNIT: &str = "B";

static DEFAULT_PSS_POLICY: PssPolicy = PssPolicy::Off;

static SHM_FS_MOUNTPOINT: &str = "/dev/shm"; // It seems the /dev/shm file system of type tmpfs is
                                             // _the_ mountpoint for shared memory, and that other
                                             // tmpfs file systems like /tmp (on some systems)
                                             // and /run have no relevance for shared memory(?),
                                             // but it's difficult to find authoritative
                                             // documentation about.

#[derive(Clone, Debug, PartialEq)]
enum PssPolicy {
    Off,
    Try,
    Required,
}

impl Default for PssPolicy {
    fn default() -> Self {
        DEFAULT_PSS_POLICY.clone()
    }
}

#[derive(Clone, Debug, Serialize)]
pub struct Opts {
    unit: MemUnit,
    format: OutFormat,

    #[serde(skip_serializing)]
    pss: PssPolicy,

    debug: bool,

    #[serde(skip_serializing)]
    maybe_user: Option<User>,
}

impl Default for Opts {
    fn default() -> Self {
        Self {
            unit: MemUnit::default(),
            format: output::DEFAULT_OUTPUT_FORMAT_VAL.clone(),
            pss: PssPolicy::default(),
            debug: false,
            maybe_user: None,
        }
    }
}

fn parse_args() -> ArgMatches {
    command!() // requires `cargo` feature
        .about(ABOUT)
        .version(crate_version!())
        .arg(
            arg!(-d --debug "Print debug messages in the console")
                .value_parser(value_parser!(bool)),
        )
        .arg(
            arg!(-f --format <format> "Output format")
                .value_parser(clap::builder::PossibleValuesParser::new([
                    "csv_header",
                    "csv",
                    "hr",
                    "hr_ts",
                    "json",
                    "oneline_ts",
                    "oneline",
                ]))
                .default_value(output::DEFAULT_OUTPUT_FORMAT_STRING),
        )
        .arg(
            arg!(-i --interval <msec> "Repeat output with pauses of msec milliseconds in between")
                .value_parser(value_parser!(u64)),
        )
        .arg(
            arg!(-p --pss <policy> "Gather PSS values")
                .value_parser(clap::builder::PossibleValuesParser::new(["try", "require"])),
        )
        .arg(
            arg!(-u --unit <unit> "Unit")
                .value_parser(clap::builder::PossibleValuesParser::new([
                    "B", "KiB", "MiB", "GiB",
                ]))
                .default_value(DEFAULT_OUTPUT_UNIT),
        )
        .arg(arg!([username] "A single username").index(1))
        .get_matches()
}

fn summarize(opts: &Opts) {
    let ps = procfs::process::all_processes().expect("Could not get a process list");
    let mut sumres = SumResult {
        opts: (*opts).clone(),
        ..Default::default()
    };
    for p in ps {
        dbg(opts.debug, format!("process: {p:?}"));
        if p.is_err() {
            // Process probably gone at this point
            continue;
        }
        let p = p.unwrap(); // Should be OK due to the above check
        if opts.maybe_user.is_some() {
            let proc_uid = p
                .uid() // type: ProcResult<u32>
                .expect("Should not be possible: Could not get UID for a process");
            if let Some(ref target_user) = opts.maybe_user {
                if proc_uid != Into::<u32>::into(target_user.uid) {
                    continue;
                }
            }
        }
        let p_status = p.status();
        if p_status.is_err() {
            // Process probably gone at this point
            continue;
        }
        let p_status = p_status.unwrap();
        sumres.num_procs += 1;
        if opts.debug {
            let msg = format!("p_status: {p_status:?}");
            dbg(opts.debug, msg);
        }

        if let Some(vmhwm_kib) = p_status.vmhwm {
            sumres.vmhwm_b += vmhwm_kib * 1024;
        }
        if let Some(vmrss_kib) = p_status.vmrss {
            sumres.vmrss_b += vmrss_kib * 1024;
        }
        if let Some(rssanon_kib) = p_status.rssanon {
            sumres.rssanon_b += rssanon_kib * 1024;
        }
        if let Some(rssfile_kib) = p_status.rssfile {
            sumres.rssfile_b += rssfile_kib * 1024;
        }
        if let Some(rssshmem_kib) = p_status.rssshmem {
            sumres.rssshmem_b += rssshmem_kib * 1024;
        }
        if let Some(hugetlbpages_kib) = p_status.hugetlbpages {
            sumres.hugetlbpages_b += hugetlbpages_kib * 1024;
        }

        // Check if we need to perform the expensive PSS stuff
        if opts.pss != PssPolicy::Off {
            let p_rollup_res = p.smaps_rollup();
            if opts.pss == PssPolicy::Required && p_rollup_res.is_err() {
                let msg = format!("Could not gather PSS data for this process: {p:?}");
                err_die(msg);
            }
            if let Ok(p_rollup) = p_rollup_res {
                // We know this will be OK, due to above check,
                // but it seems we need to check again.
                let mmaps = p_rollup.memory_map_rollup.0;
                let mmaps_len = mmaps.len();
                if mmaps_len != 1 {
                    // Extra check in case the writer has misunderstood the structures involved
                    // here
                    err_die("mmaps length is {mmaps_len} which is unexpected");
                }
                if let Some(mmap) = mmaps.first() {
                    let ext_map = &mmap.extension.map;
                    if let Some(pss_b) = ext_map.get("Pss") {
                        let oldval = sumres.pss_b.unwrap_or(0);
                        sumres.pss_b = Some(oldval + pss_b);
                    }
                    if let Some(pss_anon_b) = ext_map.get("Pss_Anon") {
                        let oldval = sumres.pss_anon_b.unwrap_or(0);
                        sumres.pss_anon_b = Some(oldval + pss_anon_b);
                    }
                    if let Some(pss_file_b) = ext_map.get("Pss_File") {
                        let oldval = sumres.pss_file_b.unwrap_or(0);
                        sumres.pss_file_b = Some(oldval + pss_file_b);
                    }
                    if let Some(pss_shmem_b) = ext_map.get("Pss_Shmem") {
                        let oldval = sumres.pss_shmem_b.unwrap_or(0);
                        sumres.pss_shmem_b = Some(oldval + pss_shmem_b);
                    }
                }
            }
        }
    }

    sumres.dev_shm_b = summarize_dev_shm_b(opts);

    present_result(sumres);
}

fn summarize_dev_shm_b(opts: &Opts) -> u64 {
    let mut file_bytes = 0;
    dbg(opts.debug, "Entered summarize_dev_shm_kib".to_string());
    for entry in WalkDir::new(SHM_FS_MOUNTPOINT)
        .into_iter()
        .filter_map(|e| e.ok())
    {
        if opts.debug {
            let msg = format!("entry: {entry:?}");
            dbg(opts.debug, msg);
        }
        if let Ok(md) = entry.metadata() {
            if !md.is_file() {
                continue;
            }
            if opts.debug {
                let msg = format!("metadata: {md:?}");
                dbg(opts.debug, msg);
            }
            if let Some(target_user) = &opts.maybe_user {
                if target_user.uid.as_raw() != md.uid() {
                    continue;
                }
            }
            file_bytes += md.len();
        }
    }
    file_bytes
}

fn err_die<S: AsRef<str> + std::fmt::Display>(msg: S) {
    let errmsg = format!("Error: {msg}");
    eprintln!("{errmsg}");
    std::process::exit(1);
}

fn determine_user(remaining: Option<clap::parser::Values<String>>) -> Option<User> {
    let maybe_username = match remaining {
        Some(mut username_args) => {
            let num_remaining = username_args.len();
            if num_remaining > 1 {
                eprintln!("Only one username is supported");
                std::process::exit(1);
            }
            Some(username_args.next().unwrap()) // We know for sure there is exactly one element, so unwrap is OK
        }
        None => None,
    };
    let maybe_user = maybe_username.map(|username| User::from_name(username.as_str()));
    maybe_user.as_ref()?; // Clippys alternative to "if maybe_user.is_none() { return None; }"
    let maybe_user = maybe_user.unwrap();

    if maybe_user.is_err() {
        eprintln!("System error lookin up user");
        std::process::exit(1);
    }
    let maybe_user = maybe_user.unwrap();

    if maybe_user.is_none() {
        eprintln!("No such user");
        std::process::exit(1);
    }

    maybe_user
}

// Handle result of Clap's handling of --unit
fn determine_unit(maybe_unit: Option<String>) -> MemUnit {
    let unit_res: Result<MemUnit, ()> = match maybe_unit {
        Some(unit) => MemUnit::from_str(unit.as_str()),
        None => Ok(MemUnit::default()),
    };
    unit_res.unwrap() // unwrap is OK, because Clap has checked the value
}

// Handle result of Clap's handling of --format
fn determine_format(maybe_format: Option<String>) -> OutFormat {
    match maybe_format {
        Some(format) => {
            if format == "csv_header" {
                OutFormat::CsvHeader
            } else if format == "csv" {
                OutFormat::Csv
            } else if format == "hr" {
                OutFormat::Hr
            } else if format == "hr_ts" {
                OutFormat::HrTs
            } else if format == "json" {
                OutFormat::Json
            } else if format == "oneline_ts" {
                OutFormat::OnelineTs
            } else {
                OutFormat::Oneline
            }
        }
        None => OutFormat::default(),
    }
}

fn dbg(do_debug: bool, msg: String) {
    if do_debug {
        println!("{msg}");
    }
}

fn main() {
    let mut args = parse_args();
    let interval: Option<u64> = args.remove_one("interval");
    let unit: MemUnit = determine_unit(args.remove_one("unit"));
    let format: OutFormat = determine_format(args.remove_one("format"));
    let def_unit = MemUnit::from_str(DEFAULT_OUTPUT_UNIT)
        .expect("A unit conversion issue happened which sould be impossible");
    if (format == OutFormat::Csv || format == OutFormat::CsvHeader || format == OutFormat::Json)
        && unit != def_unit
    {
        let msg = format!("JSON output unit must always be {}", DEFAULT_OUTPUT_UNIT);
        err_die(msg);
    }

    let maybe_pss: Option<String> = args.remove_one("pss");
    let pss = match maybe_pss {
        Some(some_pss) => {
            if some_pss == "try" {
                PssPolicy::Try
            } else if some_pss == "require" {
                PssPolicy::Required
            } else {
                PssPolicy::Off
            }
        }
        None => DEFAULT_PSS_POLICY.clone(),
    };

    let debug = args.remove_one("debug").unwrap_or(false);
    let remaining: Option<clap::parser::Values<String>> = args.remove_many("username");
    let maybe_user = determine_user(remaining);
    let opts = Opts {
        unit,
        format,
        pss,
        debug,
        maybe_user,
    };

    dbg(debug, format!("opts: {opts:?}"));
    if let Some(msec) = interval {
        let i_duration = Duration::from_millis(msec);
        loop {
            summarize(&opts);
            sleep(i_duration);
        }
    } else {
        summarize(&opts);
    }
}
