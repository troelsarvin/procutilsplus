use super::*;
use assert_cmd::Command;

#[test]
fn test_good_args() {
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .assert()
        .success();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("root")
        .assert()
        .success();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("--version")
        .assert()
        .success();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("-V")
        .assert()
        .success();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("--unit")
        .arg("KiB")
        .assert()
        .success();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("-u")
        .arg("GiB")
        .assert()
        .success();
}

#[test]
fn test_bad_args() {
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("-")
        .assert()
        .failure();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("oureosfrisyufwerysdjfhiwjrehwesdfwerwersfdwrewfs")
        .assert()
        .failure();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("-¤")
        .assert()
        .failure();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("--unit")
        .assert()
        .failure();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("--unit")
        .arg("foo")
        .assert()
        .failure();
    let _ = Command::cargo_bin("mem-per-user")
        .unwrap()
        .arg("-u")
        .arg("foo")
        .assert()
        .failure();
}

#[test]
fn test_json() {
    let sumres = SumResult {
        opts: Opts::default(),
        num_procs: 235,
        vmhwm_b: 4081960 * 1024,
        vmrss_b: 3847028 * 1024,
        rssanon_b: 1407544 * 1024,
        rssfile_b: 2358692 * 1024,
        rssshmem_b: 80792 * 1024,
        hugetlbpages_b: 0 * 1024,
        dev_shm_b: 161 * 1024,
        pss_b: Some(1784541184),
        pss_anon_b: Some(1314533376),
        pss_file_b: Some(429171712),
        pss_shmem_b: Some(40823808),
    };
    let ts_string = "2024-01-01T14:45:09.80".to_string();
    let jsumres = JsonSumResult { ts_string, sumres };
    let expected_json = "{\"ts\":\"2024-01-01T14:45:09.80\",\"num_procs\":235,\"vmhwm_b\":4179927040,\"vmrss_b\":3939356672,\"rssanon_b\":1441325056,\"rssfile_b\":2415300608,\"rssshmem_b\":82731008,\"hugetlbpages_b\":0,\"dev_shm_b\":164864,\"pss_b\":1784541184,\"pss_anon_b\":1314533376,\"pss_file_b\":429171712,\"pss_shmem_b\":40823808}".to_string();
    let res_json = serde_json::to_string(&jsumres).unwrap();
    assert_eq!(expected_json, res_json);
}
