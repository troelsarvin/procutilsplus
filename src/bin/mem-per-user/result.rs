use serde::Serialize;
use std::fmt;

use crate::output::{fmt_hr, fmt_oneline, get_timestamp_formatted, OutFormat};
use crate::Opts;

#[derive(Default, Serialize)]
pub struct SumResult {
    #[serde(skip_serializing)]
    pub opts: Opts,

    pub num_procs: usize,
    pub vmhwm_b: u64,
    pub vmrss_b: u64,
    pub rssanon_b: u64,
    pub rssfile_b: u64,
    pub rssshmem_b: u64,
    pub hugetlbpages_b: u64,
    pub dev_shm_b: u64,
    pub pss_b: Option<u64>,
    pub pss_anon_b: Option<u64>,
    pub pss_file_b: Option<u64>,
    pub pss_shmem_b: Option<u64>,
}

#[derive(Serialize)]
pub struct JsonSumResult {
    #[serde(rename = "ts")]
    pub ts_string: String,

    #[serde(flatten)]
    pub sumres: SumResult,
}

impl Default for JsonSumResult {
    fn default() -> Self {
        Self {
            ts_string: get_timestamp_formatted(),
            sumres: SumResult::default(),
        }
    }
}

impl From<SumResult> for JsonSumResult {
    fn from(sumres: SumResult) -> Self {
        Self {
            ts_string: get_timestamp_formatted(),
            sumres,
        }
    }
}

impl fmt::Display for SumResult {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.opts.format {
            OutFormat::Hr => fmt_hr(f, self, false),
            OutFormat::HrTs => fmt_hr(f, self, true),
            OutFormat::Oneline => fmt_oneline(f, self, false),
            OutFormat::OnelineTs => fmt_oneline(f, self, true),
            _ => Err(fmt::Error), // It should not be possible to end up here due
                                  // to Clap's checks
        }
    }
}
