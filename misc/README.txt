Building for any recent Debian/Ubuntu
=====================================

Assumptions:
 - Your Git clone exists in /home/troels/git-clones/procutilsplus (adjust as needed).
 - Version: 1.0.4

cd /home/troels/git-clones/procutilsplus/misc
podman build . # generates an image IIIIIIIIIIIIIIII
podman run --security-opt label=disable -v "/home/troels/git-clones/procutilsplus":/var/tmp/work -w /var/tmp/work IIIIIIIIIIIIIIII cargo deb
podman run --security-opt label=disable -v "/home/troels/git-clones/procutilsplus":/var/tmp/work -w /var/tmp/work IIIIIIIIIIIIIIII cargo generate-rpm

(The "--security-opt label=disable" argument seems to be needed when building
on a Fedora workstation with SELinux turned on.)

Assuming the above resulted in container CCCCCCCCCCCCC, copy resulting deb file to your
workstation's /var/tmp directory:
podman cp CCCCCCCCCCCCC:/var/tmp/work/target/debian/procutilsplus_1.0.4-1_amd64.deb /var/tmp/
podman cp CCCCCCCCCCCCC:/var/tmp/work/target/generate-rpm/procutilsplus-1.0.4-1.x86_64.rpm /var/tmp/

The resulting binaries should run on
 - Debian 10+
 - Ubuntu 19+
 - RHEL 8+
